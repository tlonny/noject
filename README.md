# Noject

A pain-free solution for writing in-line SQL safely:

```python
from noject import Builder
build = Builder()

max_value = 15
name = "foobar"
allowed_ids = [1, 2, 3]

sql, params = build(lambda col: f"""
    SELECT *
    FROM tbl
    WHERE value < {col(max_value)}
    AND name = {col(name)}
    AND allowed_ids IN ({",".join(col(x) for x in allowed_ids)})
""")
cur.execute(sql, params)
```

### 1. Installation

Install Noject using:

```
pip install noject
```

### 2. Setting up

Once installed, we must import and instantiate the `Builder`:

```python
from noject import Builder
build = Builder()
```

### 3. Writing queries

Now we can start writing queries! The `build` callable defined above accepts a single `fragment` as an argument. 

A `fragment` is simply a function that takes a `collector` and returns a SQL string.

A `collector` is a utility that collects all values that need to be escaped and replaces them with safe SQL parameters:

```python
def fragment(col):
    return f"SELECT * FROM tbl WHERE id = {col(5)}"

query, params = build(fragment) 
```

The result of calling the `build` function on the `fragment` is a tuple. The first element is the correctly escaped SQL query,
and the second is a dictionary mapping parameters used in the query to their values.

### 4. Nesting queries

Nesting fragments is incredibly simple, simply pass them to the `collector` as you would with normal values:

```python
def nested_fragment(col):
    return f"SELECT {col(5)}"

def fragment(col):
    return f"SELECT * FROM ({col(nested_fragment)})"
```

### 5. Changing parameter formatting

If you wish to modify how the `Builder` formats parameters within the query, simply pass a `formatter` to the constructor:

```python
build = Builder(lambda param: f":{param})
```

N.B. The default formatting is of the form: `%(param_n)s`.
